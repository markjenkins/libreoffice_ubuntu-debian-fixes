Put
Microsoft_VC110_CRT_x86.msm
into this directory for Windows builds using a VS 2012 / VC 11.0 compiler.
For builds with --enable-dbgutil also put
Microsoft_VC110_DebugCRT_x86.msm
here.
