/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This file incorporates work covered by the following license notice:
 *
 *   Licensed to the Apache Software Foundation (ASF) under one or more
 *   contributor license agreements. See the NOTICE file distributed
 *   with this work for additional information regarding copyright
 *   ownership. The ASF licenses this file to you under the Apache
 *   License, Version 2.0 (the "License"); you may not use this file
 *   except in compliance with the License. You may obtain a copy of
 *   the License at http://www.apache.org/licenses/LICENSE-2.0 .
 */

#ifndef com_sun_star_sheet_addin_XPricingFunctions_idl
#define com_sun_star_sheet_addin_XPricingFunctions_idl

#include <com/sun/star/uno/XInterface.idl>
#include <com/sun/star/beans/XPropertySet.idl>

module com
{
module sun
{
module star
{
module sheet
{
module addin
{
    /**
     * Interface with pricing functions.
     */
    interface XPricingFunctions : com::sun::star::uno::XInterface
    {
        /// opt_barrier()
        double getOpt_barrier( [in] double spot, [in] double vol,
            [in] double r, [in] double rf, [in] double T, [in] double strike,
            [in] double barrier_low, [in] double barrier_up,
            [in] double rebate,
            [in] string put_call, [in] string in_out,
            [in] string continuous, [in] any greek  )
            raises( com::sun::star::lang::IllegalArgumentException );

        /// opt_touch()
        double getOpt_touch( [in] double spot, [in] double vol,
            [in] double r, [in] double rf, [in] double T,
            [in] double barrier_low, [in] double barrier_up,
            [in] string for_dom, [in] string in_out,
            [in] string continuous, [in] any greek  )
            raises( com::sun::star::lang::IllegalArgumentException );

        /// opt_prob_hit()
        double getOpt_prob_hit( [in] double spot, [in] double vol,
            [in] double mu, [in] double T,
            [in] double barrier_low, [in] double barrier_up )
            raises( com::sun::star::lang::IllegalArgumentException );

        /// opt_prob_inmoney()
        double getOpt_prob_inmoney( [in] double spot, [in] double vol,
            [in] double mu, [in] double T,
            [in] double barrier_low, [in] double barrier_up,
            [in] any strike, [in] any put_call )
            raises( com::sun::star::lang::IllegalArgumentException );

    };
};
};
};
};
};

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
