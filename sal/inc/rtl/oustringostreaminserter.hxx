/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INCLUDED_RTL_OUSTRINGOSTREAMINSERTER_HXX
#define INCLUDED_RTL_OUSTRINGOSTREAMINSERTER_HXX

// depreacted header...  include ustring.hxx instead
#ifndef CPPUNIT_VERSION // protect agaisnt WaE with include all_headers in qa of strings
#warning "deprecated header, include rtl/ustring.hxx instead"
#endif
#include <rtl/ustring.hxx>

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
