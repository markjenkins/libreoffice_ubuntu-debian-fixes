/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of the LibreOffice project.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SFX2_TEMPLATEABSTRACTVIEW_HXX__
#define __SFX2_TEMPLATEABSTRACTVIEW_HXX__

#include <sfx2/thumbnailview.hxx>

//template thumbnail item defines
#define TEMPLATE_ITEM_MAX_WIDTH 192
#define TEMPLATE_ITEM_MAX_HEIGHT 160
#define TEMPLATE_ITEM_PADDING 5
#define TEMPLATE_ITEM_SPACE 30
#define TEMPLATE_ITEM_MAX_TEXT_LENGTH 20
#define TEMPLATE_ITEM_THUMBNAIL_MAX_HEIGHT 128

//template thumbnail image defines
#define TEMPLATE_THUMBNAIL_MAX_HEIGHT 128 - 2*TEMPLATE_ITEM_PADDING
#define TEMPLATE_THUMBNAIL_MAX_WIDTH TEMPLATE_ITEM_MAX_WIDTH - 2*TEMPLATE_ITEM_PADDING

class TemplateView;
class SfxDocumentTemplates;

enum FILTER_APPLICATION
{
    FILTER_APP_NONE,
    FILTER_APP_WRITER,
    FILTER_APP_CALC,
    FILTER_APP_IMPRESS,
    FILTER_APP_DRAW
};

// Display template items depending on the generator application
class ViewFilter_Application
{
public:

    ViewFilter_Application (FILTER_APPLICATION App)
        : mApp(App)
    {}

    bool operator () (const ThumbnailViewItem *pItem);

private:

    FILTER_APPLICATION mApp;
};

class ViewFilter_Keyword
{
public:

    ViewFilter_Keyword (const OUString &rKeyword)
        : maKeyword(rKeyword)
    {}

    bool operator () (const ThumbnailViewItem *pItem);

private:

    OUString maKeyword;
};

class SFX2_DLLPUBLIC TemplateAbstractView : public ThumbnailView
{
public:

    TemplateAbstractView (Window *pParent, WinBits nWinStyle, bool bDisableTransientChildren);

    TemplateAbstractView ( Window* pParent, const ResId& rResId, bool bDisableTransientChildren = false );

    virtual ~TemplateAbstractView ();

    // Fill view with template folders thumbnails
    virtual void Populate () = 0;

    virtual void reload () = 0;

    virtual void filterTemplatesByApp (const FILTER_APPLICATION &eApp) = 0;

    virtual void showOverlay (bool bVisible) = 0;

    void setItemDimensions (long ItemWidth, long ThumbnailHeight, long DisplayHeight, int itemPadding);

    sal_uInt16 getOverlayRegionId () const;

    const OUString& getOverlayName () const;

    // Check if the overlay is visible or not.
    bool isOverlayVisible () const;

    void deselectOverlayItems ();

    void deselectOverlayItem (const sal_uInt16 nItemId);

    void sortOverlayItems (const boost::function<bool (const ThumbnailViewItem*,
                                                       const ThumbnailViewItem*) > &func);

    virtual void filterTemplatesByKeyword (const OUString &rKeyword);

    void setOverlayItemStateHdl (const Link &aLink) { maOverlayItemStateHdl = aLink; }

    void setOverlayDblClickHdl (const Link &rLink);

    void setOverlayCloseHdl (const Link &rLink);

    static BitmapEx scaleImg (const BitmapEx &rImg, long width, long height);

    static BitmapEx fetchThumbnail (const OUString &msURL, long width, long height);

protected:

    virtual void Resize();

    virtual void Paint( const Rectangle& rRect );

    virtual void DrawItem (ThumbnailViewItem *pItem);

    virtual void OnSelectionMode (bool bMode);

    DECL_LINK(OverlayItemStateHdl, const ThumbnailViewItem*);

protected:

    TemplateView *mpItemView;
    Link maOverlayItemStateHdl;
};

#endif // __SFX2_TEMPLATEABSTRACTVIEW_HXX__

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
